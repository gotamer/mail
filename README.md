gotamer/mail/send
=================
[![GoDoc Send](https://godoc.org/bitbucket.org/gotamer/mail/send?status.svg)](https://godoc.org/bitbucket.org/gotamer/mail/send)

Sending mail with sendmail or smtp

gotamer/mail/send implements the io.Writer interface.  

gotamer/mail/imap
=================
[![GoDoc Imap](https://godoc.org/bitbucket.org/gotamer/mail/imap?status.svg)](https://godoc.org/bitbucket.org/gotamer/mail/imap)

gotamer/mail/imap to get mail from an imap server  

	


________________________________________________________

#### The MIT License (MIT)

Copyright © 2012-2013 Dennis T Kaplan <http://www.robotamer.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
